# Export a STL file, for use in 3d printing #

### Usage ###

1. Select solids to export.
2. Set scale.
3. change filename (if needed)
4. Export.


### Workspace / Nodes ###

![Figure 1. Worspace](img/export_sdl_workspace.png)


