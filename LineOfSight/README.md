# Line of Sight Modelling in Dynamo #

* [Overview](#id_overview)
* [Usage](#id_usage)
1. [Seating Plat familes](#id_plat)
* [Installation](#id_install)
1. [Downloads](#id_downloads)
* [Python Code](#id_python)

## Overview <a id=id_overview></a> ##

A slightly more involved Dynamo script.
This will model stadium seating plats, correct to this formula:


\\(N = (R + C)(D + T) - R) / D\\)

	N = riser or step height 
	R = vertical rise from focus (includes personal eye height)
	C = C value
	D = horizontal distance from focus
	T = seat (tread) width

![line_of_sight.png](img/line_of_sight.png)


## Usage <a id=id_usage></a> ##

These are the steps required to create a model:

1. Create a new conceptual mass
2. Load in a seating plat family (link)
3. Open Dynamo and load "LineOfSight.dyn (link)
3. Adjust the following seetings.
	* Number or rows required
	* Tread
	* Horizontal distance from focus
	* Vertical rise from focus
	* Eye Height
	* Min Step Height
	* Min C Value
	* Min Step Increment
	* Plat family (link)
5. Click "Run"

### Creating seating plat families <a id=id_plat></a> ###

This script can be extended to create any form.  
To do this you need to create a seating plat family.

The family must contain the following instance parameters:

* Length
* Thickness.
* Riser
* Going
* Radius (can be null for rectangular plats)

![required family parameters for the seating plat](img/dyn_line_of_sight_plat.png)


## Screen Shots ##

![Dynamo workspace with required elements highlighted](img/dyn_line_of_sight.png) 

![Dynamo workspace, with a circular seating plat family](img/dyn_line_of_sight2.png)


## Internal python code <a id=id_python></a> ##

The *RiserSequence* node contains the majority of the calculations. Here it is:

~~~ {.python}
# Default imports
import clr
clr.AddReference('RevitAPI')
clr.AddReference('RevitAPIUI')
from Autodesk.Revit.DB import *
import Autodesk
import sys
import clr
path = r'C:\Autodesk\Dynamo\Core'
exec_path = r'C:\Autodesk\Dynamo\Core\dll'
sys.path.append(path)
sys.path.append(exec_path)
clr.AddReference('LibGNet')
from Autodesk.LibG import *

#The input to this node will be stored in the IN variable.
dataEnteringNode = IN

#global vars
treadSize = IN[1]
eyeHeight = IN[4]
xDistanceToFirstRow = IN[2]
yDistanceToFirstRow = IN[3]
numberOfRows = IN[0]
minimumRiserHeight = IN[5]
minimumCValue = IN[6]
riserIncrement = IN[7]

# row vars
CValue = []
EyeToFocusX = []
RiserHeight = []
HeightToFocus = []
Going  = []
EyeHeight  = []
outList = []

for i in xrange(0, numberOfRows):
	EyeToFocusX.insert(i, xDistanceToFirstRow + (i * treadSize))
	RiserHeight.insert(i, minimumRiserHeight)
	HeightToFocus.insert(i, yDistanceToFirstRow + eyeHeight)
	Going.insert(i, treadSize)
	EyeHeight.insert(i, eyeHeight)
	CValue.insert(i, 0)
	if 0 &lt; i:
		RiserHeight[i] = minimumRiserHeight
		HeightToFocus[i] = HeightToFocus[i - 1] + minimumRiserHeight
		while True:
			CValue.insert(i - 1, ((EyeToFocusX[i - 1] * (HeightToFocus[i - 1] + RiserHeight[i])) / (EyeToFocusX[i - 1] + treadSize)) - HeightToFocus[i - 1])
			if CValue[i-1] &gt;= minimumCValue:
				break
			RiserHeight.insert(i, RiserHeight[i] + riserIncrement)
			HeightToFocus.insert(i, HeightToFocus[i] + riserIncrement)
	outList.insert(i, HeightToFocus[i] - eyeHeight)
	outList.insert(i + numberOfRows, RiserHeight[i])

#Assign your output to the OUT variable
OUT = outList
~~~


## Installation <a id=id_install></a> ##

### Download Links <a id=id_downloads></a> ###

[Download Dynamo](http://autodeskvasari.com/dynamo)

[Download Line-Of-Sight Dynamo Script](https://bitbucket.org/anicholas/dynscripts/raw/master/LineOfSight/LineOfSight.dyn)

[Rectangular Seating Plat](http://autodeskvasari.com/dynamo)

[Radial Seating Plat](http://autodeskvasari.com/dynamo)


