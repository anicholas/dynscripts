# Random Materials in Dynamo #

Here's my second attempt at using Dynamo in Revit.
This script(workspace) randomizes the material assigned to a selected family.

![Figure 1. Dynamo random material output](img/dyn_random.png)


To use it you can do this:

1. Open Dynamo, and
2. Select the family from the drop down list
3. Type in the materials that you want to use.

![Figure 2. Dynamo random material worspace](img/dyn_random2.png)

### Downloads ###

[Download Dynamo](http://autodeskvasari.com/dynamo)

[Download Random Material Dynamo Script](https://bitbucket.org/anicholas/dynscripts/raw/master/RandomMaterial/RandomMaterial.dyn)
